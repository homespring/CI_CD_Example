FROM openjdk:16
EXPOSE 8080
ADD /target/*.jar com.sidorov.igor.ci_cd-1.0-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "com.sidorov.igor.ci_cd-1.0-SNAPSHOT.jar"]
CMD ["-start"]