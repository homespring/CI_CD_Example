package com.spring.project.Service;

import org.springframework.stereotype.Service;

@Service
public class CoronaService {
    public Boolean isCorona(int temperature, int age) {
        return temperature > 37 && age > 18;
    }
}
