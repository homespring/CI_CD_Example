package com.spring.project.Controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.spring.project.Service.CoronaService;

@RestController
@RequestMapping(value = "/api/v1")
@RequiredArgsConstructor
public class CoronaController {
    @Autowired
    private CoronaService coronaService;

    @GetMapping(value = "/corona")
    public String isCorona(@RequestParam Integer temperature, @RequestParam Integer age) {
        return coronaService.isCorona(temperature, age).toString() + " Hello World";
    }
}
