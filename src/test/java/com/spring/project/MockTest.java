package com.spring.project;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class MockTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @SneakyThrows
    public void isCoronaTrue() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/corona?temperature={1}&age={2}", 38, 27))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("true Hello World"));
    }

    @SneakyThrows
    @Test
    public void isCoronaFalse() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/corona?temperature={1}&age={2}", 36, 17))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("false Hello World"));
    }
}
